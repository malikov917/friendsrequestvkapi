package my.com.friendsrequest.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class MyDataBase {

    private static final String DB_NAME = "mydatabase";
    private static final int DB_VERSION = 1;
    private static final String DB_TABLE = "mytable";

    private static final String ID_COLUMN = "_id";
    public static final String WEB_COLUMN = "web";
    public static final String TEXT_COLUMN = "txt";

    static final String DB_CREATE = "create table " + DB_TABLE + "(" +
            ID_COLUMN + " integer primary key autoincrement, " +
            WEB_COLUMN + " text, " +
            TEXT_COLUMN  + " text " +
            ");";

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private final Context context;
    private String imageUri;

    public MyDataBase(Context context){
        this.context = context;
    }

    public void open(){
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void close(){
        if(dbHelper != null){
            dbHelper.close();
        }
    }

    public Cursor readAllDataBase(){
        return sqLiteDatabase.query(DB_TABLE, null, null, null, null, null, null);
    }

    public void addRecord(String imageUri, String text){
        this.imageUri = imageUri;
        ContentValues values = new ContentValues();
        values.put(TEXT_COLUMN, text);
        values.put(WEB_COLUMN, imageUri);
        sqLiteDatabase.insert(DB_TABLE, null, values);
    }

    public void deleteRecord(long id){
        sqLiteDatabase.delete(DB_TABLE,ID_COLUMN + " = " + id, null);
    }

    public void clearAllDataBase(){
        sqLiteDatabase.delete(DB_TABLE, null, null);
    }
}
