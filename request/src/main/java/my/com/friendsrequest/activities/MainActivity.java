package my.com.friendsrequest.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import my.com.friendsrequest.*;
import my.com.friendsrequest.utils.HttpUtils;
import my.com.friendsrequest.utils.Parser;
import my.com.friendsrequest.utils.RandomGenerator;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class MainActivity extends AppCompatActivity {
    InterstitialAd mInterstitialAd;
    private TextView resTextView;
    private EditText input;
    private AdView mAdView;

    public static String url;
    private String friendsRequestType;
    private String firstFriendsParam;
    private String secondFriendsParam;
    private String thirdFriendsParam;
    private String fourthFriendsParam;

    private String userId;
    private String order;
    private String friendsFields;
    public static String nameCase;

    public static String arrayName;
    private String firstName;
    private String lastName;
    private String response;

    private ArrayList<Friend> friendsList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        input = (EditText) findViewById(R.id.user_id_edit_text);
        Button randomButton = (Button) findViewById(R.id.random_button);
        Button getFriendsButton = (Button) findViewById(R.id.user_friends_button);
        Button myIdButton = (Button) findViewById(R.id.my_id_button);
        resTextView = (TextView) findViewById(R.id.result_text_view);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                friendsList.clear();
                resTextView.setText("");
                userId = input.getText().toString();
                AsyncTask<String, Void, String> asyncTask = new MyTask().execute();
                try {
                    asyncTask.get();
                    friendsList = Parser.parseMethod(response, arrayName, firstName, lastName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (friendsList.size() == 0) {
                    resTextView.setText(getResources().getString(R.string.in_case_of_empty_list));
                    return;
                }
                if (friendsList.size() > 5000) {
                    resTextView.setText(getResources().getString(R.string.in_case_of_overload_list));
                    return;
                }
                Intent intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent.putExtra("Friends", friendsList);
                startActivity(intent);
            }
        });

        requestNewInterstitial();





        requestNewInterstitial();


        input.setOnClickListener(inputFieldClickListener);
        randomButton.setOnClickListener(randomButtonClick);
        myIdButton.setOnClickListener(myIdButtonClick);
        getFriendsButton.setOnClickListener(getFriendsClick);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //Hide keyboard
                    if (getCurrentFocus() != null) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    }
                }
                return false;
            }
        });

        url = getResources().getString(R.string.url);

        /**
         * Friends.get params
         */
        friendsRequestType = HttpUtils.RequestType.GET_FRIENDS.toString();
        firstFriendsParam = HttpUtils.FriendsGetParamsName.USER_ID.toString();
        secondFriendsParam = HttpUtils.FriendsGetParamsName.ORDER.toString();
        thirdFriendsParam = HttpUtils.FriendsGetParamsName.FIELDS.toString();
        fourthFriendsParam = HttpUtils.FriendsGetParamsName.NAME_CASE.toString();
        friendsFields = getResources().getString(R.string.friends_fields);

        order = getResources().getString(R.string.order);
        nameCase = getResources().getString(R.string.name_case);
        /**
         * Parser's params
         */
        arrayName = Parser.Parameters.ARRAY_NAME.toString();
        firstName = Parser.Parameters.NAME.toString();
        lastName = Parser.Parameters.SURNAME.toString();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.data_base_menu_button:
                Intent intent = new Intent(MainActivity.this, DataBaseActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    View.OnClickListener inputFieldClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            resTextView.setText("");
            input.setText("");
        }
    };

    View.OnClickListener myIdButtonClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

                resTextView.setText("");
                String myId = getResources().getString(R.string.my_id);
                input.setText(myId);

        }
    };

            View.OnClickListener randomButtonClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resTextView.setText("");
                    input.setText(RandomGenerator.generator());
                }
            };

            class MyTask extends AsyncTask<String, Void, String> {

                @Override
                protected String doInBackground(String... params) {
                    response = null;
                    try {
                        response = HttpUtils.makeRequest(url, friendsRequestType, firstFriendsParam,
                                secondFriendsParam, thirdFriendsParam, fourthFriendsParam, userId, order,
                                friendsFields, nameCase);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return response;
                }
            }

            View.OnClickListener getFriendsClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mInterstitialAd.isLoaded()) {
                        mInterstitialAd.show();
                    } else {
                        friendsList.clear();
                        resTextView.setText("");
                        userId = input.getText().toString();
                        AsyncTask<String, Void, String> asyncTask = new MyTask().execute();
                        try {
                            asyncTask.get();
                            friendsList = Parser.parseMethod(response, arrayName, firstName, lastName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (friendsList.size() == 0) {
                            resTextView.setText(getResources().getString(R.string.in_case_of_empty_list));
                            return;
                        }
                        if (friendsList.size() > 5000) {
                            resTextView.setText(getResources().getString(R.string.in_case_of_overload_list));
                            return;
                        }
                        Intent intent = new Intent(MainActivity.this, ListViewActivity.class);
                        intent.putExtra("Friends", friendsList);
                        startActivity(intent);
                    }
                }


            };




        private void requestNewInterstitial() {
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                    .build();

            mInterstitialAd.loadAd(adRequest);
        }

    };
