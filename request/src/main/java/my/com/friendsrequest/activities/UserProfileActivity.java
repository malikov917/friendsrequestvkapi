package my.com.friendsrequest.activities;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.*;
import android.widget.*;


import my.com.friendsrequest.*;
import my.com.friendsrequest.utils.MyDataBase;

public class UserProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private String name;
    private String surname;
    private static String photo;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.user_activity_name));

        WebView webView = (WebView) findViewById(R.id.my_web_view);
        TextView nameTV = (TextView) findViewById(R.id.tv_name);
        TextView surnameTV = (TextView) findViewById(R.id.tv_surname);
        TextView bdateTV = (TextView) findViewById(R.id.tv_bdate);
        TextView cityNameTV = (TextView) findViewById(R.id.tv_city);

        intent = getIntent();
        photo = intent.getStringExtra("photo");
        name = intent.getStringExtra("name");
        surname = intent.getStringExtra("surname");
        String bDate = intent.getStringExtra("bDate");
        String city = intent.getStringExtra("city");

        webView.loadUrl(photo);
        nameTV.setText("First Name: " + name);
        surnameTV.setText("Last Name: " + surname);
        if(bDate != null) {
            bdateTV.setText("Date of birth: " + bDate);
        }else{
            bdateTV.setText("Date of birth: hidden" );
        }
        if(city != null){
            cityNameTV.setText("City: " + city);
        }else{
            cityNameTV.setText("City: hidden");
        }

        Button addToDBBtn = (Button)findViewById(R.id.add_db_button);
        addToDBBtn.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.data_base_menu_button:
                intent = new Intent(UserProfileActivity.this, DataBaseActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        MyDataBase dataBase = new MyDataBase(this);
        dataBase.open();
        dataBase.addRecord(photo, name + " " + surname);
    }

}

