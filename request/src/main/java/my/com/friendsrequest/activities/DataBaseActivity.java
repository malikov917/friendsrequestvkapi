package my.com.friendsrequest.activities;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.LoaderManager.*;
import android.support.v4.content.*;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.*;
import android.view.ContextMenu.*;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;


import my.com.friendsrequest.*;
import my.com.friendsrequest.utils.*;


public class DataBaseActivity extends AppCompatActivity implements LoaderCallbacks<Cursor>{

    private MyDataBase myDataBase;
    private static final int CM_DELETE_ID = 1;
    private DBAdapter dbAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_base);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("DataBase");

        myDataBase = new MyDataBase(this);
        myDataBase.open();

        String[] fromDB = new String[]{MyDataBase.WEB_COLUMN, MyDataBase.TEXT_COLUMN};
        int[]toListView = new int[]{R.id.db_image_item, R.id.db_item_text_view};
        dbAdapter = new DBAdapter(this, R.layout.database_list_item, null, fromDB, toListView, 0);
        ListView dbListView = (ListView) findViewById(R.id.db_list_view);
        dbListView.setAdapter(dbAdapter);

        //add context menu to listView
        registerForContextMenu(dbListView);

        //loader for reading data
        getSupportLoaderManager().initLoader(0, null, this);

        Button clearRecordsBtn = (Button) findViewById(R.id.clear_all_records_btn);
        clearRecordsBtn.setOnClickListener(clearAllRecords);
    }

    View.OnClickListener clearAllRecords = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            myDataBase.clearAllDataBase();
            dbAdapter.notifyDataSetChanged();
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo info){
        super.onCreateContextMenu(menu, v, info);
        menu.add(0, CM_DELETE_ID, 0, R.string.delete_record);
    }

    public boolean onContextItemSelected(MenuItem item){
        if(item.getItemId() == CM_DELETE_ID){
            AdapterView.AdapterContextMenuInfo adapterContextMenuInfo =
                    (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            myDataBase.deleteRecord(adapterContextMenuInfo.id);
            getSupportLoaderManager().getLoader(0).forceLoad();
            dbAdapter.notifyDataSetChanged();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    protected void onDestroy(){
        super.onDestroy();
        myDataBase.close();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(this, myDataBase);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        dbAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    public static class MyCursorLoader extends CursorLoader {

        MyDataBase myDataBase;

        public MyCursorLoader(Context context, MyDataBase dataBase) {
            super(context);
            this.myDataBase = dataBase;
        }

        @Override
        public Cursor loadInBackground() {
            Cursor cursor = myDataBase.readAllDataBase();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cursor;
        }
    }
}
